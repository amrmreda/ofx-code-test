using System.Collections.Generic;

namespace BookingSystem.Bookings {

    public interface IBatch {
         IEnumerable<BookingRequest> BookingRequests { get; }
         void AddBookingRequest(BookingRequest bookingRequest);
         void AddBookingRequest(string bookingRequestInput);
    }
}