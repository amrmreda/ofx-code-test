using System;
using System.Collections.Generic;
using BookingSystem.Bookings.Parser;

namespace BookingSystem.Bookings {

    /// <summary>
    /// Responsable of sorting and grouping all the booking requests by submission date
    /// </summary>
    public class Batch : IBatch 
    {
        private readonly SortedSet<BookingRequest> _bookingRequests;
      
        private readonly IBookingRequestParser _parser;

        public Batch() : this(new BookingRequestParser())
        {
        }

        public Batch(IBookingRequestParser parser)
        {
            this._bookingRequests = new SortedSet<BookingRequest>();
            this._parser = parser;
        }

        public IEnumerable<BookingRequest> BookingRequests
        {
            get { return _bookingRequests; }
        }

        public void AddBookingRequest(BookingRequest bookingRequest)
        {
            if (bookingRequest == null)
            {
                throw new ArgumentNullException("bookingRequest");
            }

            _bookingRequests.Add(bookingRequest);
        }

        public void AddBookingRequest(string bookingRequestInput)
        {
            if (bookingRequestInput.Trim().Equals(string.Empty))
            {
                throw new ArgumentException("bookingRequest");
            }

            var bookingRequest = _parser.Parse(bookingRequestInput);
            AddBookingRequest(bookingRequest);
        } 
    }
}