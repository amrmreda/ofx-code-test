 using BookingSystem.Bookings;
 
 
 namespace BookingSystem.Bookings.Parser
{
    public interface IBookingRequestParser
    {
        BookingRequest Parse(string bookingRequestInput);
    }
}