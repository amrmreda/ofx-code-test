using System;
using System.Globalization; 
using BookingSystem.Bookings;

namespace BookingSystem.Bookings.Parser {
    public class BookingRequestParser : IBookingRequestParser
    { 
        private readonly string _dateFormat = "yyyy-MM-dd hh:mm:ss";

        public BookingRequestParser()
        { 
        }

        public BookingRequestParser(string dateFormat)
        {
            _dateFormat = dateFormat;
        }

        public BookingRequest Parse(string bookingRequestInput)
        {
            var lines = bookingRequestInput.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length != 3)
            {
                throw new FormatException("Invalid booking request input format.");
            }

            var firstLine = lines[0];
            var firstLineValues = firstLine.Split(' ');

            if (firstLineValues.Length != 2)
            {
                throw new FormatException("Invalid booking request input format.");
            }

            var submissionTimeValue = firstLineValues[0] + ' ' + firstLineValues[1];
            var submissionTime = DateTime.ParseExact(submissionTimeValue, _dateFormat, CultureInfo.InvariantCulture);

            var employeeIdValue = lines[1];
            var employeeId = employeeIdValue;


            var secondLine = lines[2];
            var secondLineValues = secondLine.Split(' ');
            if (secondLineValues.Length != 3)
            {
                throw new FormatException("Invalid booking request input format.");
            }

            var meetingDurationValue = secondLineValues[2];
            var meetingDuration = int.Parse(meetingDurationValue);

            var meetingSartTimeValue = secondLineValues[0] + ' ' + secondLineValues[1];
            var meetingStartTime = DateTime.Parse(meetingSartTimeValue);

            return new BookingRequest(submissionTime, employeeId, meetingDuration, meetingStartTime);
        }  
    }
}