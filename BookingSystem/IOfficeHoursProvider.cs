using System;

namespace BookingSystem {

    public interface IOfficeHoursProvider {
        TimeSpan StartTime(DayOfWeek dayOfWeek);
        TimeSpan EndTime(DayOfWeek dayOfWeek);
    }
}