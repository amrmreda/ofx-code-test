﻿using System;
using BookingSystem;
using BookingSystem.Calendars;
using Moq;
using Xunit;

namespace BookingSystem.Tests.Calendars
{ 
    public class CalendarTests
    {
        private Calendar _calendar;
        private Mock<IOfficeHoursProvider> _mockOfficeHoursProvider;
 
        public CalendarTests()
        {
            _mockOfficeHoursProvider = new Mock<IOfficeHoursProvider>();
            _mockOfficeHoursProvider.Setup(provider => provider.StartTime(It.IsAny<DayOfWeek>())).Returns(new TimeSpan(9, 0, 0));
            _mockOfficeHoursProvider.Setup(provider => provider.EndTime(It.IsAny<DayOfWeek>())).Returns(new TimeSpan(17, 30, 0));

            _calendar = new Calendar(_mockOfficeHoursProvider.Object);
        }

        [Fact]
        public void CreateMeeting_AvailableDateAndWithinOfficeHours_CalendarHasOneMeeting()
        {
            var meetingDate = new DateTime(2013, 9, 20);
            var startTime = new TimeSpan(13, 0, 0); // 1PM or 13HS
            int duration = 2; // 2 HS
            var employeeId = "EMP001";
        
            Assert.Empty(_calendar.Meetings(meetingDate));

            _calendar.CreateMeeting(meetingDate, startTime, duration, employeeId);

            var today = DateTime.Now;
            Assert.Empty(_calendar.Meetings(today)); //No meetings for today
            Assert.NotEmpty(_calendar.Meetings(meetingDate));
        }

        [Fact]
        public void CalendarDay()
        {
            var date20130930 = new DateTime(2013, 9, 30);
            var calendarDay = _calendar.CalendarDay(date20130930);

            Assert.Equal(2013, calendarDay.Year);
            Assert.Equal(9, calendarDay.Month);
            Assert.Equal(30, calendarDay.Day);
        }

      
    }
}
