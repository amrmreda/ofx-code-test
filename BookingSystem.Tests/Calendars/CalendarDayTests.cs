﻿using System;
using System.Linq;
using BookingSystem.Calendars;
using Xunit;

namespace BookingSystem.Tests.Calendars
{ 
    public class CalendarDayTests
    {
        private CalendarDay _calendarDay;
 
        public CalendarDayTests()
        {
            var calendarDayDate = new DateTime(2013, 9, 30);
            _calendarDay = new CalendarDay(calendarDayDate, new TimeSpan(9, 0, 0), new TimeSpan(17, 30, 0));
        }

        [Fact]
        public void Meetings_NoBookings_EmptyListOfMeetings()
        {
            Assert.Empty(_calendarDay.Meetings);
        }

        [Fact]
        public void Book_OneBookingBetweenOfficeHours_OneMeeting()
        {
            Assert.Empty(_calendarDay.Meetings);
            _calendarDay.Book(Hour(12), Hour(13), "EMP001");

            const int expectedMeetings = 1;
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());
        }

        [Fact]
        public void Book_OneBookingOutsideOfficeHours_EmptyListOfMeetings()
        {
            Assert.Empty(_calendarDay.Meetings);

            _calendarDay.Book(Hour(18), Hour(19), "EMP001");
            Assert.Empty(_calendarDay.Meetings);

            _calendarDay.Book(Hour(16), Hour(18), "EMP002"); //Starts Within office hours, finishes outside
            Assert.Empty(_calendarDay.Meetings);
        }

        [Fact]
        public void Book_FiveBookingsOverlapped_OneMeeting()
        {
            const int expectedMeetings = 1;
            Assert.Empty(_calendarDay.Meetings);

            _calendarDay.Book(Hour(13), Hour(16), "EMP001");
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());

            _calendarDay.Book(Hour(14), Hour(15), "EMP002"); //Within
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());

            _calendarDay.Book(Hour(15), Hour(17), "EMP003"); //Upper Boundary
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());

            _calendarDay.Book(Hour(12), Hour(14), "EMP004"); //Lower Boundary
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());

            _calendarDay.Book(Hour(12), Hour(17), "EMP005"); //Complete Overlap
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());

            _calendarDay.Book(Hour(13), Hour(16), "EMP006"); //Exact Same Time
            Assert.Equal(expectedMeetings, _calendarDay.Meetings.Count());
        }

        [Fact]
        public void Year()
        {
            const int expectedYear = 2013;
            Assert.Equal(expectedYear, _calendarDay.Year);
        }

        [Fact]
        public void Month()
        {
            const int expectedMonth = 9; //September
            Assert.Equal(expectedMonth, _calendarDay.Month);
        }

        [Fact]
        public void Day()
        {
            const int expectedDay = 30; //30th
            Assert.Equal(expectedDay, _calendarDay.Day);
        }

        private TimeSpan Hour(int hour)
        {
            return new TimeSpan(hour, 0, 0);
        }

    }
}
