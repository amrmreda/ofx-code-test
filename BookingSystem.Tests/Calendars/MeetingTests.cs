﻿using System;
using BookingSystem.Calendars;
using Xunit;

namespace BookingSystem.Calendars
{ 
    public class MeetingTests
    {
        private string _employeeId;
 
        public void SetUp()
        {
            _employeeId = "EMP001";
        }

        [Fact]
        public void CompareTo_SameDateDifferentStartTime()
        {
            var sameDate = new DateTime(2013, 9, 30);
            var newerMeeting = new Meeting(sameDate, new TimeSpan(14, 0, 0), new TimeSpan(15, 0, 0), _employeeId);

            var olderMeeting = new Meeting(sameDate, new TimeSpan(15, 0, 0), new TimeSpan(16, 0, 0), _employeeId);
            
            Assert.Equal(1, olderMeeting.CompareTo(newerMeeting));
            Assert.Equal(-1, newerMeeting.CompareTo(olderMeeting));
        }

        [Fact]
        public void CompareTo_DifferentDateSameStartTime()
        {
            var newerDate = new DateTime(2013, 9, 29);
            var sameStartTime = new TimeSpan(14, 0, 0);
            var newerMeeting = new Meeting(newerDate, sameStartTime, new TimeSpan(15, 0, 0), _employeeId);

            var olderDate = new DateTime(2013, 9, 30);
            var olderMeeting = new Meeting(olderDate, sameStartTime, new TimeSpan(15, 0, 0), _employeeId);

            Assert.Equal(1, olderMeeting.CompareTo(newerMeeting));
            Assert.Equal(-1, newerMeeting.CompareTo(olderMeeting));
        }


        /*
         * Should also test IsOverlapped and IsWithinOfficeHours methods.
         */
    }
}
