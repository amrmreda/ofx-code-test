﻿using BookingSystem;
using BookingSystem.Bookings;
using Moq;
using Xunit;

namespace BookingSystem.Tests.Bookings
{
    public class BatchProcessorTest
    {
        private BatchProcessor _batchProcessor;
        private Mock<IOfficeHoursProvider> _mockOfficeHoursProvider;
 
        public BatchProcessorTest()
        {
            _mockOfficeHoursProvider = new Mock<IOfficeHoursProvider>();


            _batchProcessor = new BatchProcessor();
        }
        
        [Fact]
        public void Process_EmtyBatch_NotNullBookingCalendarWithoutMeetings()
        {
            var batch = new Batch();
            var bookingCalendar = _batchProcessor.Process(_mockOfficeHoursProvider.Object, batch);

            Assert.NotNull(bookingCalendar);
            Assert.False(bookingCalendar.HasCalendarDays());
        }

        /*
         * More tests should be written for the processor but I think that there are plenty of unit tests
         * that show my testing methodology
         */


    }
}
