﻿using System;
using System.Linq;
using BookingSystem.Bookings;
using BookingSystem.Bookings.Parser;
using Moq;
using Xunit;

namespace BookingSystem.Tests.Bookings
{ 
    public class BatchTest
    { 
        private Batch _batch;
        private Mock<IBookingRequestParser> _mockBookingRequestInputParser;
        private string _employeeId;

        
        public BatchTest()
        {
            _mockBookingRequestInputParser = new Mock<IBookingRequestParser>();
            _batch = new Batch(_mockBookingRequestInputParser.Object);

            _employeeId = "EMP001";
        }

        [Fact]
        public void AddBookingRequest_NullBookingRequest_ThrowsArgumentNullException()
        { 
            BookingRequest bookingRequest = null;

            Assert.Throws<ArgumentNullException>(() =>  _batch.AddBookingRequest(bookingRequest));
        }

        [Fact] 
        public void AddBookingRequest_EmptyStringInput_ThrowsException()
        {
            string bookingRequest = string.Empty;
            
            Assert.Throws<ArgumentException>(() =>  _batch.AddBookingRequest(bookingRequest));
        }

        [Fact]
        public void AddBookingRequest_SecondBookingRequestHasRecentSubmissionDate_SecondMustBeReturnedFirst()
        {
            Assert.Empty(_batch.BookingRequests);

            var newerSubmissionDate = new DateTime(2013, 09, 30, 17, 20, 00); //2013-09-30 17:20:00
            var newerBookingRequest = new BookingRequest(newerSubmissionDate, _employeeId, 1, DateTime.Now);
            _batch.AddBookingRequest(newerBookingRequest);
            Assert.NotEmpty(_batch.BookingRequests);


            var olderSubmissionDate = new DateTime(2013, 09, 30, 17, 15, 00); //2013-09-30 17:15:00
            var olderBookingRequest = new BookingRequest(olderSubmissionDate, _employeeId, 1, DateTime.Now);
            _batch.AddBookingRequest(olderBookingRequest);

            const int expectedBookingRequestCount = 2;
            Assert.Equal(expectedBookingRequestCount, _batch.BookingRequests.Count());

            var firstBookingRequestToBeProcessed = _batch.BookingRequests.First();
            Assert.Equal(olderBookingRequest, firstBookingRequestToBeProcessed);
        } 
    }
}
