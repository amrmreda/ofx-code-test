using System;
using Xunit;
using BookingSystem.Bookings.Parser;
using System.Diagnostics;

namespace BookingSystem.Tests
{
    
    public class BookingRequestParserTests
    {  
        private readonly string _completeInput = "2011-03-17 10:17:06" + Environment.NewLine + "EMP001" + Environment.NewLine + "2011-03-21 09:00 1";
        private BookingRequestParser _defaultParser;
 
        public BookingRequestParserTests()
        {
            _defaultParser = new BookingRequestParser();
        }
        
        [Fact]
        public void Parse_ValidSubmissionTime()
        {
            var bookingRequest = _defaultParser.Parse(_completeInput);
            var expectedSubmissionTime = new DateTime(2011, 3, 17, 10, 17, 06);
            Assert.Equal(expectedSubmissionTime, bookingRequest.SubmissionTime);
        }

        [Fact]
        public void Parse_ValidEmployeeId()
        {
            var bookingRequest = _defaultParser.Parse(_completeInput);
            var expectedEmployeeId = "EMP001";
            Assert.Equal(expectedEmployeeId, bookingRequest.EmployeeId);
        }

        [Fact]
        public void Parse_ValidMeetingDuration()
        {
            var bookingRequest = _defaultParser.Parse(_completeInput);
            const int expectedMeetingDuration = 1;
            Assert.Equal(expectedMeetingDuration, bookingRequest.MeetingDuration);
        }

        [Fact]
        public void Parse_MeetingStartTime()
        {
            //2011-03-21 09:00
            var bookingRequest = _defaultParser.Parse(_completeInput);
            var expectedMeetingStartTime = new TimeSpan(9, 0, 0);
            Assert.Equal(expectedMeetingStartTime, bookingRequest.MeetingStartTime);
        }
    }
}
